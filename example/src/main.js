import Vue from 'vue'
import App from './App'
import uThumbnail from '../../src/index.js'

Vue.use(uThumbnail)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})

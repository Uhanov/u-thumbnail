import Thumbnail from './Thumbnail'
import ThumbnailOuter from './ThumbnailOuter'

const uThumbnail = {
  install (Vue, options) {
    Vue.component('thumbnail', Thumbnail)
    Vue.component('thumbnail-outer', ThumbnailOuter)
  }
}

// if Vue is the window object, auto install it
if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(uThumbnail)
}

export default uThumbnail

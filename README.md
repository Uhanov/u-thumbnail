## u-thumbnail
vue.js thumbnail plugin

### Installing

```js
npm i u-thumbnail --D

import uThumbnail from 'u-thumbnail'
Vue.use(uThumbnail)
```

### Usage
```html
<thumbnail-outer>
  <thumbnail
    :img="pic"
    :thumb="pic"
    :thumbClasses="['someClass']"
    :linkClasses="['someClass']"
  ></thumbnail>
</thumbnail-outer>
```